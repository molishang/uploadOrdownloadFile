<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="cn.com.bean.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%pageContext.setAttribute("path", request.getContextPath()); %>
<%
	User user = (User)request.getSession().getAttribute("user");
%>
<html>
<head>
<title> 首页 </title>
<script type="text/javascript" src="${path }/static/js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${path }/static/js/upload.js"></script>
<script type="text/javascript" src="${path }/static/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${path }/static/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="${path }/static/css/home.css">
<style type="text/css">

</style>
</head>
<body>
	<div class="container">
		<div class="row" id="myName">
			<h3>当前用户：<span><%=user==null?"zhangsan":user.getName() %></span></h3>
		</div>
		<div class="row">
			<div class="left" >
				<form action="${path }/uploadServlet" method="post" class="form1" enctype="multipart/form-data">
					<label class="my-lable">文件上传</label>
					<br>
					<img alt="请选择一张图片" src="${path }/static/img/11.jpg" id="img" class="my-img">
					<input type="file" class="my-file" name="img" id="img_1_in" onchange="PreviewImage1(this,'img')"/>
					<br>
					<button type="submit" 
					<%
						if(request.getSession().getAttribute("user")== null){
							%>
							disabled="disabled" 
							<%
						}
					%>
					class="btn btn-success">确定上传</button>
				</form>
				
				
			</div>
			<div class="right">
				<form action="#" class="form2">
					<label class="my-lable">文件下载</label>
					<br>
					<c:forEach items="${images}" var="img">
						<div class="my-do">
							<img alt="" src="/myimg/${img.imgName}" class="doimg">
							<br><br>
							<button id="btn" type="button" 
								<%
									if(request.getSession().getAttribute("user")== null){
										%>
										disabled="disabled" 
										<%
									}
								%>
							 onclick="downloadFun('${img.imgName}')" class="btn btn-success">下载</button>
							<button id="btn" type="button" onclick="delFun('${img.id}')" 
								<%
									if(request.getSession().getAttribute("user")== null){
										%>
										disabled="disabled" 
										<%
									}
								%>
							class="btn btn-warning">删除</button>
							<br><br>
							<span>${img.imgName}</span>
						</div>
					</c:forEach>
				</form>
			</div>
		</div>
	</div>
</body>

<script type="text/javascript">
function downloadFun(imgName){
	window.location.href='${path }/downloadFileServlet?imgName='+imgName;
	/* $.get("${path }/downloadFileServlet",{imgName:imgName},function(msg){
		alert(msg);
	}); */ 
}
function delFun(id){
	var c = confirm("你确定删除?");
	if(c){
		window.location.href='${path }/delFileServlst?imgId='+id;	
	}
}
</script>

</html>