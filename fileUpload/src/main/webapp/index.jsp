<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%pageContext.setAttribute("path", request.getContextPath()); %>
<html>
<head>
<title> 登录页面 </title>
<script type="text/javascript" src="${path }/static/js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="${path }/static/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="${path }/static/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="${path }/static/css/index.css">
<script type="text/javascript">
var loginInfo = '<%=request.getSession().getAttribute("loginInfo")%>';
if(loginInfo=="error"){
	alert("用户名或密码错误！");
}else{
	loginInfo="";
}
</script>
</head>
<body>
	<div class="container">
		<div class="login">
			<form action="${path }/userLoginServlst" method="post" id="myform">
				<div>
					<h2>用户登录</h2>
				</div>
				<div style="margin-top: 20px;">
					<label>姓名:</label>
					<input type="text" name="name" placeholder="请输入姓名！"/>
					<span id="Iname" ></span>
				</div>
				<div style="margin-top: 20px;">
					<label>密码:</label>
					<input type="password" name="pass" placeholder="请输入密码！"/>
					<span id="Ipass" ></span>
				</div>
				<div style="margin-top: 20px;">
					<button type="button" onclick="jyMyForm()" class="btn btn-success">提交</button>
					<button type="reset" class="btn btn-warning" style="margin-left: 25px;">重置</button>
				</div>
				<div>
					<span id="loginInfo"></span>
				</div>
			</form>
		</div>	
	</div>
</body>
<script type="text/javascript">
function jyMyForm(){
	if(jyNameIsNull() && jyPassIsNull()){
		$("#myform").submit();
	} 
	
}
function jyNameIsNull(){
	var name=$("input[name='name']").val();
	if(name==null || name==""){
		$("#Iname").text("请输入用户名！");
		return false;
	}else{
		$("#Iname").text("");
		return true;
	}
}
function jyPassIsNull(){
	var pass=$("input[name='pass']").val();
	if(pass==null || pass==""){
		$("#Ipass").text("请输入密码！");
		return false;
	}else{
		$("#Ipass").text("");
		return true;
	}
}
</script>
</html>